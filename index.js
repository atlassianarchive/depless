var events = require('events'),
    fs = require('fs'),
    less = require('less'),
    path = require('path'),
    readdirp = require('readdirp');

var depless = module.exports = new events.EventEmitter();

var analyzeFile = function (file) {
  fs.readFile(file, 'utf-8', function (err, data) {
    new less.Parser({
      paths: [path.dirname(file)].concat(depless.includePath),
      filename: file
    }).parse(data, function(err, tree) {
      if (tree && tree.rules) {
        tree.rules
          .filter(function (rule) {
            return rule.path;
          })
          .map(function (rule) {
            return path.resolve(path.dirname(file), rule.path);
          })
          .forEach(function (i) {
            depless.dependencies[i] = depless.dependencies[i] || [];
            depless.dependencies[i].push(file);
          });
      }

      depless.emit('analyzed', file);
    });
  });
};

depless.analyzeDirectory = function (directory, includePath) {
  this.includePath = includePath;
  this.dependencies = {};
  this.filesQueued = 0;
  this.filesAnalyzed = 0;

  var self = this,
      files = [];

  readdirp({ root: directory, fileFilter: '*.less' })
    .on('data', function (file) {
      self.filesQueued++;
      files.push(file.fullPath);
    })
    .on('end', function () {
      files.forEach(function (file) {
        analyzeFile(file);
      });
    });

  this.on('analyzed', function (file) {
    self.filesAnalyzed++;
    if (self.filesQueued === self.filesAnalyzed) {
      console.log(JSON.stringify(self.dependencies));
    }
  });
};

