# depless

`depless` analyzes the dependencies of LESS files in a directory. It
outputs a JSON object, with the keys being the path to a file and the values
being a list of paths to files that import it.

## Installation

```
npm install -g depless
```

## Usage

```
depless [options] [directory]

Options:

  -h, --help             output usage information
  -V, --version          output the version number
  --include-path <path>  Set include paths. Separated by `:'. Use `;' on Windows.
```

If you do not provide a `directory` argument, `depless` will analyze the
current working directory.

The `--include-path` option behaves identically to its `lessc` counterpart.

## Sample output

```
{
    "/Projects/bootstrap/less/reset.less": [
        "/Projects/bootstrap/less/bootstrap.less"
    ],
    "/Projects/bootstrap/less/variables.less": [
        "/Projects/bootstrap/less/bootstrap.less",
        "/Projects/bootstrap/less/responsive.less"
    ],
    "/Projects/bootstrap/less/mixins.less": [
        "/Projects/bootstrap/less/bootstrap.less",
        "/Projects/bootstrap/less/responsive.less"
    ],
    "/Projects/bootstrap/less/scaffolding.less": [
        "/Projects/bootstrap/less/bootstrap.less"
    ],
    "/Projects/bootstrap/less/grid.less": [
        "/Projects/bootstrap/less/bootstrap.less"
    ],

    ...
}
```

## License

Apache 2.0. See LICENSE for details.

## Authors

Jon Mooring <jmooring@atlassian.com>

